package com.example.myapplication.third;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.ColorStateList;
import android.os.Bundle;
import android.view.View;

import com.example.myapplication.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.slider.Slider;

public class PaintMainActivity extends AppCompatActivity implements RGBDialog.RGBDialogListener {

    DrawSurface drawSurface;
    FloatingActionButton chooseColorButton;
    FloatingActionButton clearButton;
    FloatingActionButton changePencilSizeButton;
    FloatingActionButton eraserButton;
    FloatingActionButton pencilButton;
    Slider changePencilSizeSlider;
    private int pencilSize = 1;
    private boolean paintButtonClicked = false;
    private boolean pencilButtonClicked = false;
    private int r = 255;
    private int g = 255;
    private int b = 255;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paint_main);
        setTitle("Rysowanie");
        drawSurface = findViewById(R.id.drawSurface);
        chooseColorButton = findViewById(R.id.choosePaintColor);
        clearButton = findViewById(R.id.clearButton);
        changePencilSizeButton = findViewById(R.id.changePencilSize);
        changePencilSizeSlider = findViewById(R.id.pencilSize);
        eraserButton = findViewById(R.id.eraserButton);
        pencilButton = findViewById(R.id.pencilButton);
        chooseColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawSurface.clear();
            }
        });
        changePencilSizeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pencilButtonClicked = !pencilButtonClicked;
                if(pencilButtonClicked) {
                    changePencilSizeSlider.setVisibility(View.VISIBLE);
                }
                else
                    changePencilSizeSlider.setVisibility(View.INVISIBLE);
            }
        });
        changePencilSizeSlider.addOnSliderTouchListener(new Slider.OnSliderTouchListener() {
            @Override
            public void onStartTrackingTouch(@NonNull  Slider slider) {

            }

            @Override
            public void onStopTrackingTouch(@NonNull Slider slider) {
                drawSurface.setPencilSize(pencilSize);
            }
        });
        changePencilSizeSlider.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                pencilSize = (int) value;
            }
        });
        eraserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawSurface.eraser();
            }
        });
        pencilButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawSurface.setPreviousPencilColor();
            }
        });
    }
    public void setColor(int red, int green, int blue){
        r = red;
        g = green;
        b = blue;
        int color = android.graphics.Color.rgb(r, g, b);
        drawSurface.setPaintColor(color);
        chooseColorButton.setBackgroundTintList(ColorStateList.valueOf(color));
    }
    private void openDialog() {
        RGBDialog dialog = new RGBDialog();
        dialog.setRGB(r,g,b);
        dialog.show(getSupportFragmentManager(), "dialog");
    }
    @Override
    protected void onResume() {
        super.onResume();
        drawSurface.startDraw();
    }

    @Override
    protected void onPause() {
        super.onPause();
        drawSurface.pauseDraw();
    }
}