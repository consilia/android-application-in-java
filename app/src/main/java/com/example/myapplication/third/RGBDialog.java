package com.example.myapplication.third;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.example.myapplication.R;
import com.google.android.material.slider.Slider;

public class RGBDialog extends AppCompatDialogFragment {

    private Slider rColor;
    private Slider gColor;
    private Slider bColor;
    private RGBDialogListener rgbDialogListener;
    ImageView colorPreview;
    private int r = 255;
    private int g = 255;
    private int b = 255;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable  Bundle savedInstanceState) {
        if(savedInstanceState != null){
            r = savedInstanceState.getInt("r");
            g = savedInstanceState.getInt("g");
            b = savedInstanceState.getInt("b");
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.rgb, null);
        builder.setView(view)
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        rgbDialogListener.setColor(r,g,b);
                    }
                });
        rColor = view.findViewById(R.id.R);
        gColor = view.findViewById(R.id.G);
        bColor = view.findViewById(R.id.B);
        colorPreview = view.findViewById(R.id.colorPreview);
        colorPreview.setBackgroundColor(android.graphics.Color.rgb(r, g, b));
        rColor.setValue(r);
        gColor.setValue(g);
        bColor.setValue(b);
        rColor.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                r = (int) value;
                colorPreview.setBackgroundColor(android.graphics.Color.rgb(r, g, b));
            }
        });
        gColor.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                g = (int) value;
                colorPreview.setBackgroundColor(android.graphics.Color.rgb(r, g, b));
            }
        });
        bColor.addOnChangeListener(new Slider.OnChangeListener() {
            @Override
            public void onValueChange(@NonNull Slider slider, float value, boolean fromUser) {
                b = (int) value;
                colorPreview.setBackgroundColor(android.graphics.Color.rgb(r, g, b));
            }
        });
        return builder.create();
    }
    public void setRGB(int red, int green, int blue){
        r = red;
        g = green;
        b = blue;
    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        rgbDialogListener = (RGBDialogListener) context;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("r",r);
        outState.putInt("g",g);
        outState.putInt("b",b);

    }

    public interface RGBDialogListener{
        void setColor(int red, int green, int blue);
    }
}
