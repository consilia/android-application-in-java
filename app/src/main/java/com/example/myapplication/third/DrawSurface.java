package com.example.myapplication.third;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

import java.util.ArrayList;

public class DrawSurface extends SurfaceView implements SurfaceHolder.Callback, Runnable{

    private final SurfaceHolder surfaceHolder;
    private Paint paint;
    private Path path;
    private Thread drawingThread;
    private final Object block = new Object();
    private boolean isThreadWorking = false;
    private Bitmap bitmap = null;
    private int currentColor = Color.BLACK;

    public DrawSurface(Context context, AttributeSet attrs) {
        super(context, attrs);

        surfaceHolder = getHolder();
        surfaceHolder.addCallback(this);
        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(currentColor);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(5);
    }

    public void setPaintColor(int color){
        paint.setColor(color);
        currentColor = color;
    }
    public void setPencilSize(int width){
        paint.setStrokeWidth(width);
    }
    public void setPreviousPencilColor(){
        paint.setColor(currentColor);
    }
    public void eraser(){
        paint.setColor(Color.WHITE);
    }
    public void clear(){
        Canvas canvas = new Canvas(bitmap);
        canvas.drawARGB(255,255,255,255);
    }
    public void startDraw(){
        drawingThread = new Thread(this);
        isThreadWorking = true;
        drawingThread.start();
    }
    public void pauseDraw(){
        isThreadWorking = false;
    }
    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        bitmap = Bitmap.createBitmap(getWidth(),getHeight(),Bitmap.Config.ARGB_8888);
        Canvas canvas = holder.lockCanvas();
        canvas.setBitmap(bitmap);
        canvas.drawARGB(255,255,255,255);
        holder.unlockCanvasAndPost(canvas);
        startDraw();
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
        bitmap = Bitmap.createScaledBitmap(bitmap,width,height,true);
    }
    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        isThreadWorking = false;
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        performClick();
        synchronized (block){
            Canvas canvas = new Canvas(bitmap);
            float xPos = event.getX();
            float yPos = event.getY();
            switch (event.getAction()){
                case MotionEvent.ACTION_DOWN:
                    canvas.drawCircle(xPos,yPos,10, paint);
                    path = new Path();
                    path.moveTo(xPos,yPos);
                    break;
                case MotionEvent.ACTION_MOVE:
                    path.lineTo(xPos,yPos);
                    canvas.drawPath(path, paint);
                    break;
                case MotionEvent.ACTION_UP:
                    canvas.drawCircle(xPos,yPos,10, paint);
                    break;
                default:
                    break;
            }
        }
        return true;
    }
    public boolean performClick(){
        return super.performClick();
    }
    @Override
    public void run() {
        while (isThreadWorking){
            Canvas canvas = null;
            try {
                synchronized (surfaceHolder) {
                    if(! surfaceHolder.getSurface().isValid()) continue;
                    canvas = surfaceHolder.lockCanvas(null);
                    synchronized (block){
                        if(isThreadWorking){
                            if(bitmap != null)
                                canvas.drawBitmap(bitmap,0,0, null);
                        }
                    }
                }
            } finally {
                if (canvas != null){
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
            try {
                Thread.sleep(1000/60);
            } catch (InterruptedException e) { }
        }
    }


}
