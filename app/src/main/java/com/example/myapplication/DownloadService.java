package com.example.myapplication;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.JobIntentService;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.nfc.tech.MifareClassic.BLOCK_SIZE;

public class DownloadService extends JobIntentService {

    private static final int JOB_ID = 1;
    private final FileInfo fileInfo = new FileInfo();

    public final static String NOTIFICATION = "com.example.intent_service.receiver";
    public final static String FILE_INFO = "info";

    public static void enqueueWork(Context context, Intent intent) {
        enqueueWork(context, DownloadService.class, JOB_ID, intent);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        InputStream webStream = null;
        FileOutputStream fileStream = null;
        String address = intent.getStringExtra("address");
        fileInfo.address = address;
        fileInfo.result = FileInfo.DOWNLOADING_CONTINUOUS;
        int downloadedBytes = 0;
        try {
            url = new URL(address);
            File currentFile = new File(url.getFile());
            File exitFile = new File(Environment.getExternalStorageDirectory() + "/Download" + File.separator + currentFile.getName().split("\\?")[0]);
            if(exitFile.exists()) exitFile.delete();
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.setDoInput(true);

            fileInfo.size = urlConnection.getContentLength();
            fileInfo.type = MimeTypeMap.getFileExtensionFromUrl(String.valueOf(url));
            DataInputStream reader = new DataInputStream(urlConnection.getInputStream());
            fileStream = new FileOutputStream(exitFile);

            byte[] buf = new byte[BLOCK_SIZE];
            int downloaded = reader.read(buf, 0 , BLOCK_SIZE);
            while(downloaded != -1){
                fileStream.write(buf,0, downloaded);
                fileInfo.downloadBytes = downloadedBytes;
                downloadedBytes += downloaded;
                downloaded = reader.read(buf, 0 , BLOCK_SIZE);
                sendMessage();
            }
            fileInfo.result = FileInfo.DOWNLOADING_OK;
        } catch (IOException e) {
            e.printStackTrace();
            fileInfo.result = FileInfo.DOWNLOADING_ERROR;
        } finally {
            if(webStream != null) {
                try {
                    webStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(urlConnection != null) urlConnection.disconnect();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        sendMessage();
    }

    private void sendMessage(){
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(FILE_INFO, fileInfo);
        sendBroadcast(intent);
    }
}
