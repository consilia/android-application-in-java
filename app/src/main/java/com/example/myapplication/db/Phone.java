package com.example.myapplication.db;

import android.view.autofill.AutofillValue;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "phone_table")
public class Phone {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    @ColumnInfo(name = "producer_name")
    public String producerName;

    @NonNull
    @ColumnInfo(name = "model_name")
    public String modelName;

    @NonNull
    @ColumnInfo(name = "android_version")
    public String androidVersion;

    @NonNull
    @ColumnInfo(name = "www_address")
    public String wwwAddress;

    public Phone(String producerName, String modelName, String androidVersion, String wwwAddress) {
        this.producerName = producerName;
        this.modelName = modelName;
        this.androidVersion = androidVersion;
        this.wwwAddress = wwwAddress;
    }

    public int getId() { return id; }

    public void setId(int id) {
        this.id = id;
    }

    public void setProducerName(@NonNull String producerName){
        this.producerName = producerName;
    }
    public String getProducerName(){
        return this.producerName;
    }
    public void setModelName(@NonNull String modelName){
        this.modelName = modelName;
    }
    public String getModelName(){
        return this.modelName;
    }
    public void setAndroidVersion(@NonNull String androidVersion){ this.androidVersion = androidVersion; }
    public String getAndroidVersion(){
        return this.androidVersion;
    }
    public void setWwwAddress(@NonNull String wwwAddress){
        this.wwwAddress = wwwAddress;
    }
    public String getWwwAddress(){
        return this.wwwAddress;
    }
}
