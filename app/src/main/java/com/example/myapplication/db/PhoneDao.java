package com.example.myapplication.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface PhoneDao {
    @Query("SELECT * FROM phone_table")
    LiveData<List<Phone>> getAll();

    @Query("SELECT * FROM phone_table WHERE id IN (:phoneId)")
    LiveData<Phone> getById(int phoneId);

    @Query("DELETE FROM phone_table")
    void deleteAll();

    @Insert
    void insert(Phone phone);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    void update(Phone phone);

    @Delete
    void delete(Phone phone);
}
