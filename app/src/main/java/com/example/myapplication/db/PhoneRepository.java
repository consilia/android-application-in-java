package com.example.myapplication.db;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class PhoneRepository {
    private PhoneDao phoneDao;
    private LiveData<List<Phone>> allPhones;

    public PhoneRepository(Application application) {
        AppDatabase db = AppDatabase.getDatabase(application);
        phoneDao = db.phoneDao();
        allPhones = phoneDao.getAll();
    }
    public LiveData<List<Phone>> getAll() {
        return allPhones;
    }
    public LiveData<Phone> getById(int phoneID){
        return phoneDao.getById(phoneID);
    }
    public void insert(Phone phone) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            phoneDao.insert(phone);
        });
    }
    public void update(Phone phone){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            phoneDao.update(phone);
        });
    }
    public void delete(Phone phone){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            phoneDao.delete(phone);
        });
    }
    public void deleteAll(){
        AppDatabase.databaseWriteExecutor.execute(() -> {
            phoneDao.deleteAll();
        });
    }
}
