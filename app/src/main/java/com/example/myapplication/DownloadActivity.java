package com.example.myapplication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Permission;
import java.util.Objects;

import static android.nfc.tech.MifareClassic.BLOCK_SIZE;

public class DownloadActivity extends AppCompatActivity  {

    private TextInputLayout addressLayout;
    private Button downloadInfoButton;
    private Button downloadFileButton;
    private TextView fileSizeLabel;
    private TextView fileSize;
    private TextView fileTypeLabel;
    private TextView fileType;
    private ProgressBar downloadProgress;
    private ProgressBar unknowSizedownloadProgress;
    private TextView progressProcent;
    private int size;
    private String type;
    private String address;
    private FileInfo fileInfo = new FileInfo();

    private final static int REQUEST_CODE_ASK_PERMISSIONS = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_download);

        addressLayout = findViewById(R.id.addressInputLayot);
        downloadInfoButton = findViewById(R.id.downloadInfoButton);
        downloadFileButton = findViewById(R.id.downloadFileButton);
        fileSizeLabel = findViewById(R.id.fileSizeLabel);
        fileSize = findViewById(R.id.fileSize);
        fileTypeLabel = findViewById(R.id.fileTypeLabel);
        fileType = findViewById(R.id.fileType);
        downloadProgress = findViewById(R.id.downloadProgress);
        progressProcent = findViewById(R.id.progressProcent);
        unknowSizedownloadProgress = findViewById(R.id.unknowSizeProgressBar);
        setListener();
    }

    public void setListener(){
        downloadInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                doStuff();
            }
        });

        downloadFileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkPermission();
            }
        });
    }



    private void checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CODE_ASK_PERMISSIONS);
        }
        else{
                Intent intent = new Intent(this, DownloadService.class);
                intent.putExtra("address", address);
                DownloadService.enqueueWork(this, intent);
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CODE_ASK_PERMISSIONS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(this, DownloadService.class);
                intent.putExtra("address", address);
                DownloadService.enqueueWork(this, intent);
            } else {
                Toast.makeText(this, "Nie można pobrać pliku bez zgody.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(DownloadService.NOTIFICATION));
    }
    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }


    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            FileInfo fileInfo = bundle.getParcelable(DownloadService.FILE_INFO);
            update(fileInfo);
        }
    };

    private void update(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
        double procent = 0.0;
        String text = "";
        if(downloadFileButton.getVisibility() == View.INVISIBLE){
            size = fileInfo.size;
            type = fileInfo.type;
            addressLayout.getEditText().setText(fileInfo.address);
            setInfo();
        }
        if(size < 0)
            unknowSizedownloadProgress.setVisibility(View.VISIBLE);
        else{
            downloadProgress.setVisibility(View.VISIBLE);
            progressProcent.setVisibility(View.VISIBLE);
        }
        if(fileInfo.result == FileInfo.DOWNLOADING_CONTINUOUS){
            if(size >  0){
                procent = (fileInfo.downloadBytes*1.0/size*100);
                text = (int) procent + "%";
                progressProcent.setText(text);
                downloadProgress.setProgress(fileInfo.downloadBytes);
            }
        }
        else {
            if(fileInfo.result == FileInfo.DOWNLOADING_OK) {
                text = 100 + "%";
                progressProcent.setText(text);
                Toast.makeText(this, "Pomyślnie pobrano plik!", Toast.LENGTH_SHORT).show();
            }
            else
                Toast.makeText(this, "Nie udało się pobrać pliku.", Toast.LENGTH_SHORT).show();
            unknowSizedownloadProgress.setVisibility(View.INVISIBLE);
            downloadProgress.setVisibility(View.INVISIBLE);
            progressProcent.setVisibility(View.INVISIBLE);
            downloadProgress.setProgress(0);
        }
    }


    private void doStuff() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                address = Objects.requireNonNull(addressLayout.getEditText()).getText().toString();
                if(address.split("\\?").length != 0){
                    address = address.split("\\?")[0] + "?raw=1";
                }
                URL url = null;
                HttpURLConnection urlConnection = null;
                try {
                    url = new URL(address);
                    urlConnection = (HttpURLConnection) url.openConnection();
                    urlConnection.setRequestMethod("GET");
                    urlConnection.setDoInput(true);
                    urlConnection.connect();
                    size = urlConnection.getContentLength();
                    type = MimeTypeMap.getFileExtensionFromUrl(String.valueOf(url));
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (urlConnection != null) urlConnection.disconnect();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setInfo();
                    }
                });
            }
        }).start();
    }
    public void setInfo(){
        if(size < 0) {
            fileSize.setText("Nie da sie okreslic");
        } else {
            String sizeText = BigDecimal.valueOf(size*1.0/1000).setScale(2, RoundingMode.HALF_UP).doubleValue() + " KB";
            fileSize.setText(sizeText);
            downloadProgress.setMax(size);
        }
        fileType.setText(type);
        fileSizeLabel.setVisibility(View.VISIBLE);
        fileTypeLabel.setVisibility(View.VISIBLE);
        fileSize.setVisibility(View.VISIBLE);
        fileType.setVisibility(View.VISIBLE);
        downloadFileButton.setVisibility(View.VISIBLE);
    }
}