package com.example.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.File;

public class FileInfo implements Parcelable {

    public final static int DOWNLOADING_OK = 0;
    public final static int DOWNLOADING_CONTINUOUS = 1;
    public final static int DOWNLOADING_ERROR = 2;

    public String address;
    public int size;
    public String type;
    public int downloadBytes;
    public int result;

    public FileInfo(){
        address = "";
        size = 0;
        type = "";
        downloadBytes = 0;
        result = 1;
    }

    public FileInfo(Parcel bundle){
        address = bundle.readString();
        size = bundle.readInt();
        type = bundle.readString();
        downloadBytes = bundle.readInt();
        result = bundle.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(address);
        dest.writeInt(size);
        dest.writeString(type);
        dest.writeInt(downloadBytes);
        dest.writeInt(result);
    }

    public static Parcelable.Creator<FileInfo> CREATOR = new Parcelable.Creator<FileInfo>(){

        @Override
        public FileInfo createFromParcel(Parcel source) {
            return new FileInfo(source);
        }

        @Override
        public FileInfo[] newArray(int size) {
            return new FileInfo[size];
        }
    };
}
