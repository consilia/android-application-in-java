package com.example.myapplication.firstApp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

import java.util.List;

public class GradesListAdapter extends RecyclerView.Adapter<GradesListAdapter.GradeViewHolder>{

    private final List<GradeModel> gradesList;

    public static class GradeViewHolder extends RecyclerView.ViewHolder {
        private TextView label;
        private RadioGroup gradesGroup;
        public GradeViewHolder(@NonNull View itemView) {
            super(itemView);
            label =  itemView.findViewById(R.id.gradesRadiosLabel);
            gradesGroup = itemView.findViewById(R.id.gradesGroup);

        }

        public TextView getLabel() {
            return label;
        }
        public RadioGroup getGradesGroup() {
            return gradesGroup;
        }

    }
    public GradesListAdapter(List<GradeModel> gradesList){
        this.gradesList = gradesList;
    }
    @NonNull
    @Override
    public GradeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grade, parent, false);
        return new GradeViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull GradesListAdapter.GradeViewHolder holder, int position) {
        GradeModel grade = gradesList.get(position);
        holder.getLabel().setText(grade.getName());
        holder.getGradesGroup().setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.grade2:{
                        grade.setGrade(2);
                        break;
                    }
                    case R.id.grade3:{
                        grade.setGrade(3);
                        break;
                    }
                    case R.id.grade4:{
                        grade.setGrade(4);
                        break;
                    }case R.id.grade5:{
                        grade.setGrade(5);
                        break;
                    }
                    default:{
                        grade.setGrade(0);
                        break;
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return gradesList.size();
    }

}
