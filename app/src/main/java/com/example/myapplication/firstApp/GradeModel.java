package com.example.myapplication.firstApp;

public class GradeModel {
    private String name;
    private int grade;
    GradeModel(String name, int grade){this.name = name; this.grade = grade;};

    public String getName() {
        return name;
    }
    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public void setName(String name) {
        this.name = name;
    }
}
