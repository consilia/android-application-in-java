package com.example.myapplication.firstApp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myapplication.R;

import java.util.ArrayList;

public class GradesForAverageActivity extends AppCompatActivity {


    private ArrayList<GradeModel> gradesList;
    private RecyclerView gradesRecyclerView;
    private GradesListAdapter gradeListAdapter;
    private Button backButton;
    private int gradesNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grades_for_average);
        setTitle("WPROWADZANIE OCEN");
        Bundle bundle = getIntent().getExtras();
        gradesNumber = bundle.getInt("quantity");

        gradesList = new ArrayList<GradeModel>();
        for(int i=1; i<=gradesNumber; i++){
            gradesList.add(new GradeModel("ocena " + Integer.toString(i), 0));
        }

        gradesRecyclerView = (RecyclerView) findViewById(R.id.gradesList);
        gradesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        gradeListAdapter = new GradesListAdapter(gradesList);
        gradesRecyclerView.setAdapter(gradeListAdapter);

        backButton = (Button) findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ifEachIsSelected()){
                    double average = calculateAverage(gradesNumber);
                    Bundle bundle2 = new Bundle();
                    bundle2.putDouble("average", average);
                    Intent intent = new Intent();
                    intent.putExtras(bundle2);
                    setResult(RESULT_OK,intent);
                    finish();
                }
                else{
                    Toast toast = Toast.makeText( GradesForAverageActivity.this, "Musisz zaznaczyc wszystkie oceny!", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }
    public double calculateAverage(int gradesNumber){
        double average;
        double sum = 0.0;
        for(int i=0; i<gradesNumber; i++){
            GradeModel grade = (GradeModel) gradesList.get(i);
            sum += grade.getGrade();
        }
        average = sum/gradesNumber;
        return average;
    }
    public boolean ifEachIsSelected(){
        GradeModel grade;
        for(int i=0; i<gradesNumber; i++){
            grade = (GradeModel) gradesList.get(i);
            if(grade.getGrade() == 0){
                return false;
            }
        }
        return true;
    }

}