package com.example.myapplication.firstApp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.myapplication.R;
import com.google.android.material.textfield.TextInputLayout;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DataForAverageActivity extends AppCompatActivity{

    private Button passButton;
    private Button endButton;
    private TextInputLayout firstNameLayout;
    private TextInputLayout lastNameLayout;
    private TextInputLayout gradesNumberLayout;
    private TextView averageText;
    double average;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_for_average);
        setTitle("WPROWADZANIE DANYCH");
        firstNameLayout = (TextInputLayout) findViewById(R.id.firstNameLayout);
        lastNameLayout = (TextInputLayout) findViewById(R.id.lastNameLayout);
        gradesNumberLayout = (TextInputLayout) findViewById(R.id.gradesNumberLayout);
        passButton = (Button) findViewById(R.id.passButton);
        averageText = (TextView) findViewById(R.id.averageText);
        endButton = (Button) findViewById(R.id.endButton);

        validation(firstNameLayout);
        validation(lastNameLayout);
        validation(gradesNumberLayout);

        passButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String grades = String.valueOf(gradesNumberLayout.getEditText().getText());
                Intent intent = new Intent(DataForAverageActivity.this, GradesForAverageActivity.class);
                intent.putExtra("quantity",Integer.parseInt(grades));
                startActivityForResult(intent,0);
            }
        });
        endButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog alertDialog = new AlertDialog.Builder(DataForAverageActivity.this).create();
                if(average > 3.0)
                    alertDialog.setMessage("Gratulacje otrzymujesz zaliczenie!");
                else
                    alertDialog.setMessage("Wysyłam podanie o zaliczenie warunkowe");
                alertDialog.setButton(Dialog.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
                alertDialog.show();
            }
        });
    }
    public void checkButton(){
        if(firstNameLayout.getEditText().length() > 0 && lastNameLayout.getEditText().length() > 0 && gradesNumberLayout.getEditText().length() > 0){
            if(TextUtils.isEmpty(firstNameLayout.getError()) && TextUtils.isEmpty(lastNameLayout.getError()) && TextUtils.isEmpty(gradesNumberLayout.getError())){
                passButton.setVisibility(View.VISIBLE);
            }
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        firstNameLayout.setEnabled(false);
        lastNameLayout.setEnabled(false);
        gradesNumberLayout.setEnabled(false);
        if(resultCode == RESULT_OK){
            Bundle bundle = data.getExtras();
            average = bundle.getDouble("average");
            average = BigDecimal.valueOf(average).setScale(2, RoundingMode.HALF_UP).doubleValue();
            passButton.setVisibility(View.INVISIBLE);
            averageText.setVisibility(View.VISIBLE);
            averageText.setText("Twoja średnia to: " + average);
            if(average > 3.0)
                endButton.setText("Super!");
            else
                endButton.setText("Tym razem nie poszło.");
            endButton.setVisibility(View.VISIBLE);
        }
    }
    public void validation(TextInputLayout l){
        l.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    checkTextInputs(l);
                }
                checkButton();
            }
        });
        l.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkTextInputs(l);
                checkButton();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void checkTextInputs(TextInputLayout l) {
        if (l.getId() == R.id.gradesNumberLayout) {
            String number = l.getEditText().getText().toString();
            if (!(l.getEditText().length() > 0) || Integer.parseInt(number) < 5 || Integer.parseInt(number) > 15) {
                l.setError("Lizba musi znajdować się w przedziale [5;15]!");
            } else
                l.setError(null);
        } else {
            if (!(l.getEditText().length() > 0))
                l.setError("Pole jest wymagane!");
            else
                l.setError(null);
        }
    }


}