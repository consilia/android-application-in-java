package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.firstApp.DataForAverageActivity;
import com.example.myapplication.secondApp.ListDbActivity;
import com.example.myapplication.third.PaintMainActivity;

public class MenuActivity extends AppCompatActivity {

    private Button firstApp;
    private Button secondApp;
    private Button thirdApp;
    private Button fourthApp;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        setTitle("");
        firstApp = (Button) findViewById(R.id.button);
        secondApp = (Button) findViewById(R.id.button2);
        thirdApp = (Button) findViewById(R.id.button3);
        fourthApp = (Button) findViewById(R.id.button4);

        firstApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, DataForAverageActivity.class);
                startActivity(intent);
            }
        });
        secondApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ListDbActivity.class);
                startActivity(intent);
            }
        });
        thirdApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, PaintMainActivity.class);
                startActivity(intent);
            }
        });
        fourthApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, DownloadActivity.class);
                startActivity(intent);
            }
        });
    }
}