package com.example.myapplication.secondApp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.example.myapplication.db.Phone;
import com.example.myapplication.db.PhoneRepository;

import java.util.List;

public class PhoneViewModel extends AndroidViewModel {
    private static PhoneRepository phoneRepository;
    private final LiveData<List<Phone>> allPhones;

    public PhoneViewModel(Application application) {
        super(application);
        phoneRepository = new PhoneRepository(application);
        allPhones = phoneRepository.getAll();
    }

    LiveData<List<Phone>> getAll() {return allPhones;}
    LiveData<Phone> getById(int phoneId) {return phoneRepository.getById(phoneId);}
    public static void insert(Phone phone) {phoneRepository.insert(phone);}
    public static void update(Phone phone) {phoneRepository.update(phone);}
    public void delete(Phone phone) {phoneRepository.delete(phone);}
    public void deleteAll() {phoneRepository.deleteAll();}

}
