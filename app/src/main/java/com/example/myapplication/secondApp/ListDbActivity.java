package com.example.myapplication.secondApp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;


import com.example.myapplication.R;
import com.example.myapplication.db.Phone;

import java.util.List;

public class ListDbActivity extends AppCompatActivity implements PhoneListAdapter.RecyclerViewClickListener, PhoneListAdapter.RecyclerViewLongClickListener {

    private PhoneViewModel phoneViewModel;
    private PhoneListAdapter phoneListAdapter;
    private List<Phone> phoneList;
    public static final int ADD_PHONE = 1;
    public static final int EDIT_PHONE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_db);
        setTitle("Lista");


        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.phoneList);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));



        phoneListAdapter = new PhoneListAdapter(this, this);
        recyclerView.setAdapter(phoneListAdapter);
        phoneViewModel = new ViewModelProvider(this).get(PhoneViewModel.class);
        phoneViewModel.getAll().observe(this, phones -> {
            phoneListAdapter.setPhoneList(phones);
            phoneList = phones;
        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_to_db_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.add_item){
            Intent intent = new Intent(ListDbActivity.this, EditProductActivity.class);
            startActivityForResult(intent,ADD_PHONE);
        }
        return true;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            Bundle bundle = data.getExtras();
            String producer = bundle.getString("producer_name");
            String model = bundle.getString("model_name");
            String android = bundle.getString("android_name");
            String www = bundle.getString("www_name");
            Phone phone = new Phone(producer, model, android, www);
            if (requestCode == ADD_PHONE){
                PhoneViewModel.insert(phone);
            }
            else if(requestCode == EDIT_PHONE){
                int id = bundle.getInt("id");
                phone.setId(id);
                PhoneViewModel.update(phone);
            }
        }
    }

    @Override
    public void OnClick(int position) {
        Phone phone = phoneList.get(position);
        Bundle bundle = new Bundle();
        bundle.putInt("id",phone.getId());
        bundle.putString("producer_name",phone.producerName);
        bundle.putString("model_name",phone.modelName);
        bundle.putString("android_name",phone.androidVersion);
        bundle.putString("www_name",phone.wwwAddress);
        Intent intent = new Intent(ListDbActivity.this, EditProductActivity.class);
        intent.putExtras(bundle);
        startActivityForResult(intent,EDIT_PHONE);
    }

    @Override
    public void OnLongClick(int position) {
        Phone phone = phoneList.get(position);
        phoneViewModel.delete(phone);
    }


}