package com.example.myapplication.secondApp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;

import com.example.myapplication.R;
import com.google.android.material.textfield.TextInputLayout;

public class EditProductActivity extends AppCompatActivity {

    private Button wwwButton;
    private Button cancelButton;
    private Button saveButton;
    private TextInputLayout producerLayout;
    private TextInputLayout modelLayout;
    private TextInputLayout androidLayout;
    private TextInputLayout wwwLayout;
    private int phoneId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_product);
        setTitle("Dodaj/edytuj dane");
        wwwButton = (Button) findViewById(R.id.wwwButton);
        cancelButton = (Button) findViewById(R.id.cancelButton);
        saveButton = (Button) findViewById(R.id.saveButton);
        producerLayout = (TextInputLayout) findViewById(R.id.producerLayout);
        modelLayout= (TextInputLayout) findViewById(R.id.modelLayout);
        androidLayout = (TextInputLayout) findViewById(R.id.androidLayout);
        wwwLayout = (TextInputLayout) findViewById(R.id.wwwLayout);

        validation(producerLayout);
        validation(modelLayout);
        validation(androidLayout);
        validation(wwwLayout);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String producerName = producerLayout.getEditText().getText().toString();
                String modelName = modelLayout.getEditText().getText().toString();
                String androidName = androidLayout.getEditText().getText().toString();
                String wwwName = wwwLayout.getEditText().getText().toString();

                Bundle bundle = new Bundle();
                bundle.putInt("id",phoneId);
                bundle.putString("producer_name",producerName);
                bundle.putString("model_name",modelName);
                bundle.putString("android_name",androidName);
                bundle.putString("www_name",wwwName);
                Intent reply = new Intent();
                reply.putExtras(bundle);
                setResult(RESULT_OK, reply);
                finish();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(EditProductActivity.this, ListDbActivity.class);
                startActivity(intent);
            }
        });
        wwwButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String address = wwwLayout.getEditText().getText().toString();
                Intent intent = new Intent(Intent.ACTION_VIEW);
                if(!address.startsWith("http://")){
                    address = "http://" + address;
                }
                intent.setData(Uri.parse(address));
                startActivity(intent);
            }
        });
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            phoneId = bundle.getInt("id");
            producerLayout.getEditText().setText(bundle.getString("producer_name"));
            modelLayout.getEditText().setText(bundle.getString("model_name"));
            androidLayout.getEditText().setText(bundle.getString("android_name"));
            wwwLayout.getEditText().setText(bundle.getString("www_name"));
        }
    }
    public void validation(TextInputLayout l){
        l.getEditText().setOnFocusChangeListener(new View.OnFocusChangeListener(){
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    checkTextInputs(l);
                    checkButton();
                }
            }
        });
        l.getEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                checkTextInputs(l);
                checkButton();
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
    public void checkTextInputs(TextInputLayout l) {
        if (!(l.getEditText().length() > 0))
            l.setError("Pole jest wymagane!");
        else
            l.setError(null);
    }
    public void checkButton(){
        if(producerLayout.getEditText().length() > 0 && modelLayout.getEditText().length() > 0 && androidLayout.getEditText().length() > 0 && wwwLayout.getEditText().length() > 0){
            if(TextUtils.isEmpty(producerLayout.getError()) && TextUtils.isEmpty(modelLayout.getError()) && TextUtils.isEmpty(androidLayout.getError()) && TextUtils.isEmpty(wwwLayout.getError())){
                saveButton.setEnabled(true);
            }
        }
        else{
            saveButton.setEnabled(false);
        }
    }
}