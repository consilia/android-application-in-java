package com.example.myapplication.secondApp;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;
import com.example.myapplication.db.Phone;

import java.util.ArrayList;
import java.util.List;



public class PhoneListAdapter extends RecyclerView.Adapter<PhoneListAdapter.PhoneViewHolder> {

    private List<Phone> phoneList = new ArrayList<>();
    private final RecyclerViewClickListener recyclerViewClickListener;
    private final RecyclerViewLongClickListener recyclerViewLongClickListener;
    private List<Phone> selectedPhoneList = new ArrayList<>();
    boolean isEnabled = false;

    public PhoneListAdapter(RecyclerViewClickListener recyclerViewClickListener, RecyclerViewLongClickListener recyclerViewLongClickListener){
        this.recyclerViewClickListener = recyclerViewClickListener;
        this.recyclerViewLongClickListener = recyclerViewLongClickListener;
    }

    public interface RecyclerViewClickListener{
        void OnClick(int position);
    }
    public interface RecyclerViewLongClickListener{
        void OnLongClick(int position);
    }
    public void setPhoneList(List<Phone> phoneList){
        this.phoneList = phoneList;
        notifyDataSetChanged();
    }
    public List<Phone> getSelectedPhoneList(){
        return selectedPhoneList;
    }
    @NonNull
    @Override
    public PhoneListAdapter.PhoneViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.phone, parent, false);
        return new PhoneViewHolder(view, recyclerViewClickListener, recyclerViewLongClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull PhoneViewHolder holder, int position) {
        Phone currentPhone = phoneList.get(position);
        holder.producerNameView.setText(currentPhone.getProducerName());
        holder.modelNameView.setText(currentPhone.getModelName());
        holder.androidNameView.setText(currentPhone.getAndroidVersion());
    }

    @Override
    public int getItemCount() {
        return phoneList.size();
    }
    public int getSelectedItemCount() { return selectedPhoneList.size(); }
    public class PhoneViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {
        private final TextView producerNameView;
        private final TextView modelNameView;
        private final TextView androidNameView;
        RecyclerViewClickListener recyclerViewClickListener;
        RecyclerViewLongClickListener recyclerViewLongClickListener;

        public PhoneViewHolder(@NonNull View itemView, RecyclerViewClickListener recyclerViewClickListener, RecyclerViewLongClickListener recyclerViewLongClickListener) {
            super(itemView);
            producerNameView = (TextView) itemView.findViewById(R.id.producerLabel);
            modelNameView = (TextView) itemView.findViewById(R.id.modelLabel);
            androidNameView = (TextView) itemView.findViewById(R.id.androidVersionLabel);
            this.recyclerViewClickListener = recyclerViewClickListener;
            this.recyclerViewLongClickListener = recyclerViewLongClickListener;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }
        @Override
        public void onClick(View v) {
            recyclerViewClickListener.OnClick(getAdapterPosition());
        }


        public void clickItem(View v){
            Phone phone = phoneList.get(getAdapterPosition());
            if(selectedPhoneList.contains(phone)){
                v.setBackgroundColor(Color.TRANSPARENT);
                selectedPhoneList.remove(phone);
            }
            else{
                v.setBackgroundColor(Color.LTGRAY);
                selectedPhoneList.add(phone);
            }
        }
        @Override
        public boolean onLongClick(View v) {
            //clickItem(v);
            //v.startActionMode(actionModeCallback);
            recyclerViewLongClickListener.OnLongClick(getAdapterPosition());
            return true;
        }
        /*private ActionMode.Callback actionModeCallback = new ActionMode.Callback() {
            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.delete_from_db_menu, menu);
                return true;
            }
            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }
            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                if (item.getItemId() == R.id.delete_item) {
                    for (Phone phone : selectedPhoneList){
                        selectedPhoneList.remove(phone);
                    }
                    mode.finish();
                }
                return true;
            }
            @Override
            public void onDestroyActionMode(ActionMode mode) {
                actionMode = null;

                selectedPhoneList.clear();
            }
        };*/


    }
}
